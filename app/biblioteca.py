from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from app.auth import login_required
from app.db import get_db

bp = Blueprint('biblioteca', __name__)

@bp.route('/')
def index():
    if g.user is not None:
        db = get_db()
        livros = db.execute(
            'SELECT l.id, l.titulo, l.edicao, l.editora, l.autores, l.idUsuario, u.username FROM livro l JOIN usuario u ON l.idUsuario = u.id WHERE l.idUsuario=?;',
            (str(g.user['id']))
        ).fetchall()
        return render_template('biblioteca/index.html', livros=livros)
    else:
        db = get_db()
        livros = db.execute(
            'SELECT * FROM livro;'
        ).fetchall()
        return render_template('app/index.html', livros=livros)

@bp.route('/create_book', methods=['GET', 'POST'])
@login_required
def create_book():
    if request.method == 'POST':
        titulo = request.form['titulo']
        edicao = request.form['edicao']
        editora = request.form['editora']
        autores = request.form['autores']
        error = None

        if not titulo:
            error = 'titulo é necessário'
        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO livro (titulo, edicao, editora, autores, idUsuario) VALUES (?,?,?,?,?);',
                (titulo, edicao, editora, autores, g.user['id'])
            )
            db.commit()
            return redirect(url_for('biblioteca.index'))
    
    return render_template('biblioteca/livro_create.html')

def get_livro(id, check_user=True):
    livro = get_db().execute(
        'SELECT l.id, l.titulo, l.edicao, l.editora, l.autores, l.idUsuario, u.username FROM livro l JOIN usuario u ON l.idUsuario = u.id WHERE l.id=?;',
        (str(id))
    ).fetchone()
    if livro is None:
        abort(404, f'Livro id {id} não encontrado.')
    if check_user and livro['idUsuario'] != g.user['id']:
        abort(403)
    
    return livro

@bp.route('/<int:id>/atualiza_livro', methods=('GET', 'POST'))
@login_required
def atualiza(id):
    livro = get_livro(id)

    if request.method == 'POST':
        titulo = request.form['titulo']
        edicao = request.form['edicao']
        editora = request.form['editora']
        autores = request.form['autores']
        error = None

        if not titulo:
            error = 'Titulo é necessário'
        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE livro SET titulo=?, edicao=?, editora=?, autores=? WHERE id=?',
                (titulo, edicao, editora, autores, id)
            )
            db.commit()
            return redirect(url_for('biblioteca.index'))
    return render_template('biblioteca/livro_atualiza.html', livro=livro)

@bp.route('/<int:id>/livro_apagar', methods=['POST'])
@login_required
def delete(id):
    get_livro(id)
    db = get_db()
    db.execute('DELETE FROM livro WHERE id=?', str(id))
    db.commit()
    return redirect(url_for('biblioteca.index'))
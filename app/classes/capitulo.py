import json

class Capitulo:
    def __init__(self, id, idlivro, pagina, cap_titulo, idusuario):
        self.id = id
        self.idlivro = idlivro
        self.pagina = pagina
        self. cap_titulo = cap_titulo
        self.idusuario = idusuario
    
    def get_json(self):
        return json.dumps(
            {
                'id': self.id,
                'idlivro': self.idlivro,
                'pagina': self.pagina,
                'cap_titulo': self.cap_titulo,
                'idusuario': self.idusuario
            }
        )

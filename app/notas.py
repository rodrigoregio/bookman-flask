from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from app.auth import login_required
from app.db import get_db

bp = Blueprint('notas', __name__)

@bp.route('/capitulo/<int:id>/notas')
@bp.endpoint('notas_capitulo')
@login_required
def notas_capitulo(id):
    db = get_db()
    notas_capitulo = db.execute(
        'SELECT n.id, n.idcapitulo, n.not_pagina, n.nota, n.idusuario, c.cap_titulo,c.idLivro FROM notas n JOIN capitulo c ON c.id=n.idcapitulo WHERE idCapitulo=?',
        (str(id))
    ).fetchall()

    if notas_capitulo != []:
        return render_template('notas/lista.html',
            notas_capitulo=notas_capitulo)
    else:
        return render_template('notas/sem_lista.html', id=str(id))

@bp.route('/capitulo/<int:id>/nova_nota', methods=['GET', 'POST'])
@bp.endpoint('notas_capitulo_criar')
@login_required
def notas_capitulo_criar(id):
    if request.method == 'POST':
        not_pagina = request.form['pagina']
        anotacao = request.form['nota']
        error = None
        if not not_pagina:
            error = 'Informe o numero da página que você quer informar'
        if not anotacao:
            error = 'Se não há nada a informar melhor não criar a anotação'
        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO notas (idCapitulo, not_pagina, nota, idUsuario) VALUES (?,?,?,?);',
                (id, not_pagina, anotacao, g.user['id'])
            )
            db.commit()
            return redirect(url_for('notas.notas_capitulo', id=str(id)))
    
    return render_template('notas/nota_create.html')

def getNota(id, check_user = True):
    nota = get_db().execute(
        'SELECT n.id, n.idCapitulo, n.not_pagina, n.nota, n.idusuario, c.cap_titulo FROM notas n JOIN capitulo c ON c.id=n.idcapitulo WHERE n.id=?;',
        (str(id),)
    ).fetchone()
    if nota is None:
        abort(404, 'Nota id {id} não foi encontrada...')
    if check_user and nota['idusuario'] != g.user['id']:
        abort(403)

    return nota

@bp.route('/capitulo/<int:idCapitulo>/nota/<int:id>/alterar', methods=('GET', 'POST'))
@bp.endpoint('notas_capitulo_alterar')
@login_required
def notas_capitulo_alterar(idCapitulo, id):
    nota = getNota(id)
    if request.method == 'POST':
        not_pagina = request.form['pag']
        nota_dado = request.form['minhanota']
        error = None

        if not nota_dado:
            error = 'Hum, interessante, você quer deixar uma nota sem uma nota, conte-me mais sobre isso...'
        if not not_pagina:
            error = 'Preciso da página para você identificar melhor.'
        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE notas SET not_pagina=?, nota=? WHERE id=? AND idcapitulo=?;',
                (not_pagina, nota_dado, id, idCapitulo)
            )
            db.commit()
            return redirect(url_for('notas.notas_capitulo', id=str(idCapitulo)))
    return render_template('notas/nota_update.html', nota=nota)

@bp.route('/capitulo/<int:idCapitulo>/nota/<int:id>/apagar', methods=['POST'])
@login_required
def delete(id, idCapitulo):
    getNota(id)
    db = get_db()
    db.execute(
        'DELETE FROM notas WHERE id=? AND idCapitulo=?;',
        (str(id), str(idCapitulo))
    )
    db.commit()
    return redirect(url_for('notas.notas_capitulo', id=str(idCapitulo)))
from flask import (Blueprint, request)
from app.db import get_db, faz_commit
from werkzeug.exceptions import abort
from app.classes.capitulo import Capitulo
import json

bp = Blueprint('api_capitulos', __name__)

@bp.route('/api/capitulos')
def getAll():
    db = get_db()
    capitulos = db.execute(
        'SELECT * FROM capitulo;'
    ).fetchall()
    recapitulos = []
    for cap in capitulos:
        l = Capitulo(
            cap['id'],
            cap['idLivro'],
            cap['pagina'],
            cap['cap_titulo'],
            cap['idusuario'],
        )
        recapitulos.append(l)
    return json.dumps(recapitulos, default=vars)

@bp.route('/api/capitulo/<int:id>', methods=['GET'])
def get_one(id):
    db = get_db()
    dcap = db.execute(
        'select * from capitulo where id=?',
        (str(id))
    ).fetchone()
    capitulo=Capitulo(dcap['id'], dcap['idLivro'], dcap['pagina'], dcap['cap_titulo'], dcap['idusuario'])
    return capitulo.get_json()

@bp.route('/api/livro/<int:idlivro>/capitulo/<int:id>', methods=['GET'])
def get_one_of_book(idlivro, id):
    db = get_db()
    dcap = db.execute(
        'select * from capitulo where id='+str(id)+' and idLivro='+str(idlivro)+';'
    ).fetchone()
    capitulo=Capitulo(dcap['id'], dcap['idLivro'], dcap['pagina'], dcap['cap_titulo'], dcap['idusuario'])
    return capitulo.get_json()

@bp.route('/api/capitulos/livro/<int:id>', methods=['GET'])
def get_all_of_book(id):
    db = get_db()
    dcaps = db.execute(
        'SELECT * FROM capitulo WHERE idLivro=?',
        (str(id))
    ).fetchall()
    recaps = []
    for cap in dcaps:
        l = Capitulo(
            cap['id'],
            cap['idLivro'], 
            cap['pagina'],
            cap['cap_titulo'],
            cap['idusuario']
        )
        recaps.append(l)
    return json.dumps(recaps, default=vars)

@bp.route('/api/livro/<int:id>/capitulo/add', methods=['POST'])
def user_add_chapter(id):
    c = Capitulo(
        0, # id do livro, mas como vou registrar ainda não o sei, por isso o 0
        id,
        request.json['pagina'],
        request.json['cap_titulo'],
        request.json['idusuario']
    )
    if c.cap_titulo is not None:
        db = get_db()
        print(c)
        db.execute(
            'INSERT INTO capitulo (idLivro, pagina, cap_titulo, idusuario) VALUES (?,?,?,?);',
            (c.idlivro, c.pagina, c.cap_titulo, c.idusuario)
        )
        faz_commit()
    else:
        return {'message':'Não realizado por falta de um título'}
    return json.dumps({'message':'Dado incluido no banco com sucesso!'})

@bp.route('/api/livro/<int:idLivro>/capitulo/<int:id>/alterar', methods=['PUT'])
def user_update_chapter(idLivro, id):
    c = Capitulo(
        id,
        idLivro,
        request.json['pagina'],
        request.json['cap_titulo'],
        request.json['idusuario']
    )
    if c.cap_titulo is not None:
        db = get_db()
        db.execute(
            'UPDATE capitulo SET cap_titulo=?, pagina=?, idusuario=? WHERE id=? AND idLivro=?;',
           (c.cap_titulo, c.pagina, c.idusuario, id, idLivro)
        )
        faz_commit()
    else:
        return { 'message': 'Não podemos enviar um livro sem título' }
    return { 'message': 'Alteração realizada com sucesso'}

@bp.route('/api/livro/<int:idLivro>/capitulo/<int:id>/apagar', methods=['DELETE'])
def user_delete_chapter(idLivro, id):
    db = get_db()
    if check_id_exists(id):
        print(check_id_exists(id))
        db.execute(
            'DELETE FROM capitulo WHERE id=? AND idLivro=?;',
            (id, idLivro)
        )
    else:
        return abort(404, 'Capítulo id {id} não foi encontrado!')
    faz_commit()
    return { 'message':'Acreditamos que foi realizado com sucesso kkkk' }

def check_id_exists(id):
    db = get_db()
    dcap = db.execute(
        "SELECT * FROM capitulo WHERE id=?",
        (str(id))
    ).fetchone()

    if dcap is not None:
        return True
    else:
        return False
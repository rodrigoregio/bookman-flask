# bookman-flask


## O que é??

O bookman é um projeto feito em Flask na linguagem Python, e é um sistema onde posso cadastrar meus livros e fazer uma nota sobre um capítulo daquele livro cadastrado. Em breve mais detalhes serão dados.

## Tá e como instalo ele??

1. Primeiro baixe o projeto com o "git clone"
git clone git@gitlab.com:boirods/bookman-flask.git
2. Acesse a pasta do projeto
cd bookman-flask
3. Crie um ambiente virtual
python -m venv venv
4. Ative este ambiente virtual
source venv/bin/activate
5. Instale o Flask no ambiente virtual
pip install flask
6. Inicie o banco de dados, isto cria todo o banco de dados da aplicação
flask --app app init-db

E pronto, agora você pode executar a aplicação com o comando
flask -app app run
